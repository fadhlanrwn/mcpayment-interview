# MCPayment-Interview
This project is intended to complete the MC Payment interview test

### Prerequisites

What things you need to install the software and how to install them

```
JAVA (ver. 1.8.0_251)
IDE (Better using Intellij 2020.1.2)
....
```

### Running the tests
```
You can test by running class MainTest.java
....
```

### Authors

* Fadhlan Ridhwanallah
