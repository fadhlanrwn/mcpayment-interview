/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mc.interview.Main;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author fadhlan
 */
public class MainTest {
    
    public MainTest() {
    }

    @Test 
    public void firstQuestion_NormalCase() {
        List<Integer> input = new ArrayList<>();
        input.add(1);
        input.add(2);
        input.add(3);
        input.add(4);
        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(4);
        List<Integer> result = Main.firstQuestion(input);
        assertEquals(expectedResult.size(), result.size());
        for(int i = 0; i < expectedResult.size(); i++) {
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }
    
    @Test 
    public void firstQuestion_NonUniqueNumberCase() {
        List<Integer> input = new ArrayList<>();
        input.add(1);
        input.add(2);
        input.add(2);
        input.add(3);
        input.add(4);
        input.add(4);
        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(4);
        expectedResult.add(4);
        List<Integer> result = Main.firstQuestion(input);
        assertEquals(expectedResult.size(), result.size());
        for(int i = 0; i < expectedResult.size(); i++) {
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }
    
    @Test 
    public void firstQuestion_NegativeNumberCase() {
        List<Integer> input = new ArrayList<>();
        input.add(1);
        input.add(2);
        input.add(-2);
        input.add(3);
        input.add(-4);
        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(3);
        List<Integer> result = Main.firstQuestion(input);
        assertEquals(expectedResult.size(), result.size());
        for(int i = 0; i < expectedResult.size(); i++) {
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }

    @Test
    public void secondQuestion_NormalCase() {
        List<Integer> input = new ArrayList<>();
        input.add(1);
        input.add(2);
        input.add(3);
        input.add(4);
        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(1);
        expectedResult.add(2);
        expectedResult.add(3);
        List<Integer> result = Main.secondQuestion(input,4);
        assertEquals(expectedResult.size(), result.size());
        for(int i = 0; i < expectedResult.size(); i++) {
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }

    @Test
    public void secondQuestion_NoMatchCase() {
        List<Integer> input = new ArrayList<>();
        input.add(1);
        List<Integer> expectedResult = new ArrayList<>();
        List<Integer> result = Main.secondQuestion(input, 1);
        assertEquals(expectedResult.size(), result.size());
        for(int i = 0; i < expectedResult.size(); i++) {
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }

    @Test
    public void thirdQuestion_NormalCase() {
        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("loud");
        expectedResult.add("four");
        expectedResult.add("lost");
        List<String> result = Main.thirdQuestion("souvenir loud four lost",4);
        assertEquals(expectedResult.size(), result.size());
        for(int i = 0; i < expectedResult.size(); i++) {
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }

    @Test
    public void thirdQuestion_NoMatchCase() {
        List<String> expectedResult = new ArrayList<>();
        List<String> result = Main.thirdQuestion("lorem ipsum",4);
        assertEquals(expectedResult.size(), result.size());
        for(int i = 0; i < expectedResult.size(); i++) {
            assertEquals(expectedResult.get(i), result.get(i));
        }
    }
}
