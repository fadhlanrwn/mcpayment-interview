/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.interview;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author fadhlan
 */
public class Main {
    public static void main(String[] args) {
    }
    
    public static List<Integer> firstQuestion(List<Integer> nums) {
        List<Integer> result = new ArrayList<>();
        Integer max = 0;
        for (int num: nums) {
            if (max < num) {
                max = num;
                result = new ArrayList<>();
                result.add(num);
            } else if (max == num) {
                result.add(num);
            }
        }
        return result;
    }

    public static List<Integer> secondQuestion(List<Integer> nums, int x) {
        List<Integer> result = new ArrayList<>();
        Set<Integer> numSet = convertListToMap(nums);
        for (int num: nums) {
            if (num % x != 0 || !numSet.contains(num / x)) {
                result.add(num);
            }
        }

        return result;
    }

    public static List<String> thirdQuestion(String word, int x) {
        List<String> result = new ArrayList<>();
        String tmp = "";
        for (int index = 0; index < word.length(); index++) {
            if (word.charAt(index) != ' ') {
                tmp = tmp + word.charAt(index);
            }

            if (word.charAt(index) == ' ' || index == word.length() - 1) {
                if (tmp.length() == x) {
                    result.add(tmp);
                }
                tmp = "";
            }
        }

        return result;
    }

    public static Set<Integer> convertListToMap(List<Integer> nums) {
        Set<Integer> result = new HashSet<>();
        for (int num: nums) {
            result.add(num);
        }

        return result;
    }
}
